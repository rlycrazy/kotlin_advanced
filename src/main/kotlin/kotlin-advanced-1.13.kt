import java.text.DecimalFormat
import java.time.LocalDate
import java.util.logging.Handler

fun typeCasting(arg: Any?) {
    println(when(arg) {
        is String -> "Я получил String = $arg, ее длина равна ${arg.length}"
        is Int -> "Я получил Int = $arg, его квадрат равен ${arg * arg}"
        is Double -> "Я получил Double = $arg, это число округляется до ${"%.2f".format(arg)} "
        is LocalDate -> "Я получил LocalDate $arg, она " +
                "${if (arg > LocalDate.of(2006,1,24)) "больше" else "меньше"} даты основания Tinkoff"
        null -> "Объект равен null"
        else -> "Мне этот тип неизвестен"
    }
    )
}

fun main() {
    typeCasting("Privet")
    typeCasting(145)
    typeCasting(145.0)
    typeCasting(145.2897812)
    typeCasting(LocalDate.of(2020,1,1))
    typeCasting(LocalDate.of(2000,1,1))
    typeCasting(Handler::class)
    typeCasting(null)
}
