fun humanData(
    name: String,
    surname: String,
    patronymic: String? = "",
    gender: String,
    age: Int,
    dateOfBirth: String,
    inn: String? = "",
    snils: String? = ""
): String {
    return "$name $surname $patronymic $gender $age $dateOfBirth $inn $snils"
}
fun main() {
   println("Со всеми параметрами " + humanData(
        name = "Igor",
        surname = "Nikolaev",
        patronymic = "Vladimirovich",
        gender = "FiveReasonMan",
        age = 63,
        dateOfBirth = "1960 - 01 - 30",
        inn = "1234567890",
        snils = "111 - 111 - 111"
    )
   )
    println("Только с обязательными параметрами " + humanData(
        name = "Igor",
        surname = "Nikolaev",
        gender = "FiveReasonMan",
        age = 63,
        dateOfBirth = "1960 - 01 - 30"
    )
    )
    println("Произвольный порядок " + humanData(
        name = "Igor",
        dateOfBirth = "1960 - 01 - 30",
        gender = "FiveReasonMan",
        age = 63,
        inn = "1234567890",
        surname = "Nikolaev",
        snils = "111 - 111 - 111",
        patronymic = "Vladimirovich"
    )
    )
}
