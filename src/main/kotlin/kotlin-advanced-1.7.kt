fun varargTask(vararg params: String) {
    println("Передано ${params.count()} элемента:")
    params.forEach { print("$it;") }
}

fun main() {
    varargTask("12","122","1234","fpo")
}