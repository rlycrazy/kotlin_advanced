fun MutableList<Int>.square() : MutableList<Int> {
    return this.map { it * it }.toMutableList()
}

fun main() {
    val list = mutableListOf(1,4,9,16,25)
    println(list.square())
}