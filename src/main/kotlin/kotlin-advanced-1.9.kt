data class Person(
    val name: String,
    val surName: String,
    val patronymic:String? = null,
    val gender: String,
    var age: Int,
    val dateOfBirth : String,
    val inn: String? = null,
    val snils: String? = null
)
fun personConfig(
    name: String,
    surName: String,
    patronymic:String? = null,
    gender: String,
    age: Int ,
    dateOfBirth : String,
    inn: String? = null,
    snils: String? = null
): Person {
    return Person(
        name,
        surName,
        patronymic,
        gender,
        age,
        dateOfBirth,
        inn,
        snils
    )
}
 fun main() {
     val person = personConfig(name = "Van" , surName = "Darkholm" , patronymic = null, gender = "Male" , age = 43, dateOfBirth = "1980-01-04", inn = null, snils = null)
     println(person.toString())
 }