package kotlinAdvanced1_11

class TestTask {
    companion object {
        var counter = 0
        fun counter() {
            counter++
            println("Вызван counter. Количество вызовов = $counter")
        }
    }
}

fun main() {
    TestTask.counter()
    TestTask.counter()
    TestTask.counter()
    TestTask.counter()
    TestTask.counter()
}